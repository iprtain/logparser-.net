﻿using System.Data.Common;
using System.Collections.Generic;
using System;

namespace parser
{
    class Program
    {
        static void Main(string[] args)
        {   
            string filename = args[0];

            Store store = new Store();

            LogParser logParser = new LogParser(store);

            logParser.parse(filename);

            LogPrinter logPrinter = new LogPrinter();

            logPrinter.printResultsTotal(store.countViewes());
            logPrinter.printResultsUnique(store.countUniqueViewes());
/*
            foreach(KeyValuePair<string, List<string>> record in store.records){
                System.Console.WriteLine(record.Key);
                foreach(string path in record.Value){
                    System.Console.WriteLine(path);
                }
                
            }
            */

            
        }
    }
}
