using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace parser{

    class LogParser{

        Store store;

        public LogParser(Store store){

            this.store = store;

        }


        public void parse(string filename){
            string fileName = @filename;
            
            using (StreamReader reader = new StreamReader(fileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    parseLine(line);
                }
            }
        }

        private void parseLine(string line){
            string[] splitedLine = line.Split(' ');
            string webpage = splitedLine.First();
            string path = splitedLine.Last();
            if(store.records.ContainsKey(webpage)){
                store.records[webpage].Add(path);
                
            }
            else{
                store.records.Add(webpage, new List<string>{path});
            }
            
        }
    }
}

