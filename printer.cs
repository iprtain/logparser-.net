using System.Collections.Generic;
namespace parser{
    class LogPrinter{

        public void printResultsTotal(Dictionary<string,int> results){
            foreach(KeyValuePair<string,int> result in results){
                System.Console.WriteLine("{0} - total number of viewes is {1}", result.Key, result.Value);
            }
        }

        public void printResultsUnique(Dictionary<string,int> results){
            foreach(KeyValuePair<string,int> result in results){
                System.Console.WriteLine("{0} -  number of unique viewes is {1}", result.Key, result.Value);
            }
        }
    }
}