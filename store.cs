using System.Collections.Generic;
using System.Linq;

namespace parser{

    class Store{

        public Dictionary<string, List<string>> records = new Dictionary<string, List<string> >();


        public Dictionary<string,int> countViewes(){
            Dictionary<string, int> counts = new Dictionary<string, int>();
            foreach(KeyValuePair<string, List<string>> record in this.records){
                counts.Add(record.Key, record.Value.Count);
            };
            var ordered = counts.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            return ordered;
        }

        

        public Dictionary<string,int> countUniqueViewes(){
             Dictionary<string, int> counts = new Dictionary<string, int>();
             foreach(KeyValuePair<string, List<string>> record in this.records){
                var unique_paths = new HashSet<string>(record.Value);
                counts.Add(record.Key, unique_paths.Count);
                
            };
            var ordered = counts.OrderByDescending(pair => pair.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            return ordered;
        }

    
    }

}

